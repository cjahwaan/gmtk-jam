﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioQue : MonoBehaviour
{
    public AudioClip clip;
    public AudioSource source;

    public void Awake()
    {
        source = GameObject.Find("Main Audio Source").GetComponent<AudioSource>();
    }

    public void playQue()
    {
        source.PlayOneShot(clip);
    }
}
