﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunManager : MonoBehaviour {

    public Gun[] guns;

    public void PullTrigger()
    {
        for (int i = 0; i < guns.Length; i++)
        {
            guns[i].PullTrigger();
        }
    }

    public void ReleaseTrigger()
    {
        for (int i = 0; i < guns.Length; i++)
        {
            guns[i].ReleaseTrigger();
        }
    }
}
