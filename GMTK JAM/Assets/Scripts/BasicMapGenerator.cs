﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RoomInfo
{
    int enemyCount;
    bool hasChest;
    bool hasCoin;
    bool hasHealth;

    public RoomInfo(int enemyCount, bool hasChest, bool hasCoin, bool hasHealth)
    {
        this.enemyCount = enemyCount;
        this.hasCoin = hasCoin;
        this.hasChest = hasChest;
        this.hasHealth = hasHealth;
    }
}

public class BasicMapGenerator : MonoBehaviour
{

    public int width, height;
    public int roomCount_Min, roomCount_Max;

    bool[,] floorPlan;
    RoomInfo[,] Floor;

    public bool[,] GetFloorPlan()
    {
        return floorPlan;
    }

    public bool isThereRoom(int x, int y)
    {
        return floorPlan[x, y];
    }

    public RoomInfo[,] GetRoomInfo()
    {
        return Floor;
    }

    public void GenerateFloor()
    {
        GenerateFloorPlan();
        GenerateRoomInfo();
    }

    void GenerateRoomInfo()
    {
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if(floorPlan[x, y])
                {
                    Floor[x, y] = new RoomInfo(Random.Range(1, 5),
                                           Random.Range(0, 100) > 90 ? true : false,
                                           Random.Range(0, 100) > 98 ? true : false,
                                           Random.Range(0, 100) > 95 ? true : false);
                }
            }
        }
    }

    void GenerateFloorPlan()
    {
        int x = width / 2, y = height / 2;
        int roomCount = Random.Range(roomCount_Min, roomCount_Max);
        floorPlan = new bool[width, height];
        Floor = new RoomInfo[width, height];
        int dir = 0;
        floorPlan[x, y] = true;
        for (int i = 0; i < roomCount; i++)
        {
            dir = chooseRandomDirection(dir);
            switch (dir)
            {
                case 1: // Up
                    if (y + 1 > height - 1)
                    {
                        x = width / 2;
                        y = height / 2;
                    }
                    y++;
                    floorPlan[x, y] = true;
                    dir = 2;
                    break;

                case 2: // Down
                    if (y - 1 < 0)
                    {
                        x = width / 2;
                        y = height / 2;
                    }
                    y--;
                    floorPlan[x, y] = true;
                    dir = 1;
                    break;

                case 3: // right
                    if (x + 1 > width - 1)
                    {
                        x = width / 2;
                        y = height / 2;
                    }
                    x++;
                    floorPlan[x, y] = true;
                    dir = 4;
                    break;

                case 4: // left
                    if (x - 1 < 0)
                    {
                        x = width / 2;
                        y = height / 2;
                    }
                    x--;
                    floorPlan[x, y] = true;
                    dir = 3;
                    break;
            }
            Debug.Log("Dir: " + dir);
        }
    }

    int chooseRandomDirection(int previous)
    {
        int r = Random.Range(1, 5);
        while (r == previous)
        {
            r = Random.Range(1, 5);
        }
        return r;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(new Vector3( (width/2) * 20, (height/2) * 20, 0), new Vector3(width * 20, height * 20, 0));

        Gizmos.color = new Color(0, 0.5f, 0.5f, 0.25f);
        Gizmos.DrawCube(new Vector3( (width/2) * 20, (height/2) * 20, 0), new Vector3(width * 20 , height * 20, 0));
    }
}
