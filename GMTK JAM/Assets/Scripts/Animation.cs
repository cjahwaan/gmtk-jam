﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AnimationState
{
    Play, Pause, End, Idle, Stop, Start
}

public class Animation : MonoBehaviour {

    public Sprite[] sprites;
    public int idle;
    public bool repeat;
    public float speed;

    int index;
    float startTime;

    SpriteRenderer spriteRenderer;
    AnimationState state;

	// Use this for initialization
	void Start ()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = sprites[0];
        state = AnimationState.Stop;
	}
	
	// Update is called once per frame
	void Update ()
    {
        switch(state)
        {
            case AnimationState.Start:
                startTime = Time.time;
                break;

            case AnimationState.Play:
                index = (int)((Time.time - startTime) * speed ) % sprites.Length;
                spriteRenderer.sprite = sprites[index];
                if (index == 0) state = AnimationState.End;
                break;

            case AnimationState.End:
                if(repeat)
                {
                    state = AnimationState.Play;
                }
                else
                {
                    state = AnimationState.Stop;
                }
                break;

            case AnimationState.Pause:
                
                break;

            case AnimationState.Idle:
                spriteRenderer.sprite = sprites[idle];
                break;

            case AnimationState.Stop:
                
                break;
        }
	}

    public void PauseAnimation()
    {
        state = AnimationState.Pause;
    }

    public void Play()
    {
        state = AnimationState.Play;
    }

    public void Idle()
    {
        state = AnimationState.Idle;
    }

    public void End()
    {
        state = AnimationState.End;
    }
}
