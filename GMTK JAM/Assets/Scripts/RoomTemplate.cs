﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomTemplate : MonoBehaviour {

    public GameObject[] one_door; //0=up, 1=down, 2=left, 3=right
    public GameObject[] two_door; //1=horizontal, 0=vertical
    public GameObject[] three_door; //0=up, 1=down, 2=left, 3=right
    public GameObject four_door;

    public int MAX_ROOM = 12;
    public int current_count = 0;

    private void Start()
    {
        int rand = Random.Range(0, 4);
        Instantiate(one_door[rand], transform.position, one_door[rand].transform.rotation);
    }
}
