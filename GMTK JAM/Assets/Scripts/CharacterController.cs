﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CharacterController : MonoBehaviour
{
    public Character character;
    public float drag;
    public string vertical, horizontal, fire;
    public GunManager gun;

    Rigidbody2D body;
    Vector2 direction;


    private void Awake()
    {
        body = GetComponent<Rigidbody2D>();
        direction = Vector3.zero;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if(character.isDead())
        {
            SceneManager.LoadScene("DeathScreen");
        }
        if (character.isDead()) this.gameObject.SetActive(false);
        if(Input.GetButton(fire))
        {
            gun.PullTrigger();
        }
        if(Input.GetButtonUp(fire))
        {
            gun.ReleaseTrigger();
        }
        direction.x = Input.GetAxis(horizontal);
        direction.y = Input.GetAxis(vertical);
	}

    private void FixedUpdate()
    {
        body.AddForce(direction * character.GetSpeed());
        body.AddForce(-body.velocity * drag);
    }
}
