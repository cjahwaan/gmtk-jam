﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChanger : MonoBehaviour
{

    public KeyCode code;
    public string sceneName;
	
	// Update is called once per frame
	void Update ()
    {
        if(Input.GetKeyDown(code))
        {
            SceneManager.LoadScene(sceneName);
        }
	}
}
