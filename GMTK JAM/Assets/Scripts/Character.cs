﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DamageMod
{
    Fire, Poison, Electric, None
}

public class Character : MonoBehaviour
{

    public int   baseHealth;
    public float baseSpeed;
    public DamageMod[] damageMod;

    float currentSpeed;
    public int maxHealth, currentHealth;


	// Use this for initialization
	void Start ()
    {
        maxHealth = currentHealth = baseHealth;
        currentSpeed = baseSpeed;
	}

    public float GetSpeed()
    {
        return currentSpeed;
    }

    public int GetCurrentHealth()
    {
        return currentHealth;
    }

    public int GetMaxHealth()
    {
        return maxHealth;
    }

    public void ResetHealth()
    {
        currentHealth = maxHealth;
    }

    public void DecHealth(int health)
    {
        currentHealth -= health;
    }

    public bool isDead()
    {
        return currentHealth <= 0;
    }
}
