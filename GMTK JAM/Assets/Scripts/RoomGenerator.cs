﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomGenerator : MonoBehaviour {
    public bool up; //0
    public bool down; //1
    public bool left; //2
    public bool right; //3

    public GameObject up_go, down_go, left_go, right_go; //spawners
    private int direction;
    RoomTemplate template;

	// Use this for initialization
	void Start () {
        template = GameObject.FindGameObjectWithTag("RoomTemplate").GetComponent<RoomTemplate>();
        Debug.Log("current_count=" + template.current_count);
        if (template.current_count > template.MAX_ROOM) return;
        bool chosen_direction = false;
        //Debug.Log("name=" + gameObject.name);
        //Debug.Log("up=" + up);
        //Debug.Log("down=" + down);
        //Debug.Log("right=" + right);
        //Debug.Log("left=" + left);
        while (!chosen_direction) //loop to determine direction
        {
            if (!up && !down && !left && !right)
            {
                return;
            }
            direction = Random.Range(0, 4);
            if ((direction == 0 && up) || (direction == 1 && down) || (direction == 2 && left) || (direction == 3 && right))
            {
                chosen_direction = true;
            }
        }
        int n_doors = 0; //number of doors to instantiate
        if (template.current_count < template.MAX_ROOM) //if it isn't the last room then randomly select from 2 to 4 doors
        {
            n_doors = Random.Range(1, 4);
        }
        else if (template.current_count == template.MAX_ROOM) //if it is last room then 1 door only
        {
            n_doors = 0;
        }
        if (direction == 0) //up
        {
            //down,left, and right should be single door if allowed
            GameObject instance = n_doors == 0 ? Instantiate(template.one_door[1], up_go.transform.position, template.one_door[1].transform.rotation) :
                n_doors == 1 ? Instantiate(template.two_door[0], up_go.transform.position, template.two_door[0].transform.rotation) :
                n_doors == 2 ? Instantiate(template.three_door[1], up_go.transform.position, template.three_door[1].transform.rotation) :
                Instantiate(template.four_door, up_go.transform.position, template.four_door.transform.rotation);
            instance.GetComponent<RoomGenerator>().down = false;
            if (left)
            {
                GameObject temp = Instantiate(template.one_door[3], left_go.transform.position, template.one_door[3].transform.rotation);
                temp.GetComponent<RoomGenerator>().right = false;
                if (template.current_count > template.MAX_ROOM) return;
                template.current_count++;
            }
            if (right)
            {
                GameObject temp = Instantiate(template.one_door[2], right_go.transform.position, template.one_door[2].transform.rotation);
                temp.GetComponent<RoomGenerator>().left = false;
                if (template.current_count > template.MAX_ROOM) return;
                template.current_count++;
            }
            if (down)
            {
                GameObject temp = Instantiate(template.one_door[0], down_go.transform.position, template.one_door[0].transform.rotation);
                temp.GetComponent<RoomGenerator>().up = false;
                if (template.current_count > template.MAX_ROOM) return;
                template.current_count++;
            }
        }
        else if (direction == 1) //down
        {
            //up, left and right should be single if allowed
            GameObject instance = n_doors == 0 ? Instantiate(template.one_door[0], down_go.transform.position, template.one_door[0].transform.rotation) :
                n_doors == 1 ? Instantiate(template.two_door[0], down_go.transform.position, template.two_door[0].transform.rotation) :
                n_doors == 2 ? Instantiate(template.three_door[0], down_go.transform.position, template.three_door[0].transform.rotation) :
                Instantiate(template.four_door, down_go.transform.position, template.four_door.transform.rotation);
            instance.GetComponent<RoomGenerator>().up = false;
            if (up)
            {
                GameObject temp = Instantiate(template.one_door[1], up_go.transform.position, template.one_door[1].transform.rotation);
                temp.GetComponent<RoomGenerator>().down = false;
                if (template.current_count > template.MAX_ROOM) return;
                template.current_count++;
            }
            if (left)
            {
                GameObject temp = Instantiate(template.one_door[3], left_go.transform.position, template.one_door[3].transform.rotation);
                temp.GetComponent<RoomGenerator>().right = false;
                if (template.current_count > template.MAX_ROOM) return;
                template.current_count++;
            }
            if (right)
            {
                GameObject temp = Instantiate(template.one_door[2], right_go.transform.position, template.one_door[2].transform.rotation);
                temp.GetComponent<RoomGenerator>().left = false;
                if (template.current_count > template.MAX_ROOM) return;
                template.current_count++;
            }
        }
        else if (direction == 2)//left
        {
            //up, down and right should be single if allowed
            GameObject instance = n_doors == 0 ? Instantiate(template.one_door[3], left_go.transform.position, template.one_door[3].transform.rotation) :
                n_doors == 1 ? Instantiate(template.two_door[1], left_go.transform.position, template.two_door[1].transform.rotation) :
                n_doors == 2 ? Instantiate(template.three_door[3], left_go.transform.position, template.three_door[3].transform.rotation) :
                Instantiate(template.four_door, left_go.transform.position, template.four_door.transform.rotation);
            instance.GetComponent<RoomGenerator>().right = false;
            if (up)
            {
                GameObject temp = Instantiate(template.one_door[1], up_go.transform.position, template.one_door[1].transform.rotation);
                temp.GetComponent<RoomGenerator>().down = false;
                if (template.current_count > template.MAX_ROOM) return;
                template.current_count++;
            }
            if (down)
            {
                GameObject temp = Instantiate(template.one_door[0], down_go.transform.position, template.one_door[0].transform.rotation);
                temp.GetComponent<RoomGenerator>().up = false;
                if (template.current_count > template.MAX_ROOM) return;
                template.current_count++;
            }
            if (right)
            {
                GameObject temp = Instantiate(template.one_door[2], right_go.transform.position, template.one_door[2].transform.rotation);
                temp.GetComponent<RoomGenerator>().left = false;
                if (template.current_count > template.MAX_ROOM) return;
                template.current_count++;
            }
        }
        else //right
        {
            //up,down and left should be single if allowed
            GameObject instance = n_doors == 0 ? Instantiate(template.one_door[2], right_go.transform.position, template.one_door[2].transform.rotation) :
                n_doors == 1 ? Instantiate(template.two_door[1], right_go.transform.position, template.two_door[1].transform.rotation) :
                n_doors == 2 ? Instantiate(template.three_door[2], right_go.transform.position, template.three_door[2].transform.rotation) :
                Instantiate(template.four_door, right_go.transform.position, template.four_door.transform.rotation);
            instance.GetComponent<RoomGenerator>().left = false;
            if (up)
            {
                GameObject temp = Instantiate(template.one_door[1], up_go.transform.position, template.one_door[1].transform.rotation);
                temp.GetComponent<RoomGenerator>().down = false;
                if (template.current_count > template.MAX_ROOM) return;
                template.current_count++;
            }
            if (down)
            {
                GameObject temp = Instantiate(template.one_door[0], down_go.transform.position, template.one_door[0].transform.rotation);
                temp.GetComponent<RoomGenerator>().up = false;
                if (template.current_count > template.MAX_ROOM) return;
                template.current_count++;
            }
            if (left)
            {
                GameObject temp = Instantiate(template.one_door[3], left_go.transform.position, template.one_door[3].transform.rotation);
                temp.GetComponent<RoomGenerator>().right = false;
                if (template.current_count > template.MAX_ROOM) return;
                template.current_count++;
            }
        }
        template.current_count++;
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        template.current_count--;
        Destroy(gameObject);
    }

}
