﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "BulletADJ/DamageBoost")]
public class DamageBoost : BulletAdj
{
    public float damageMultiplier;
    
    public override void DoDamage(GameObject target, int baseDamage)
    {
        int finalDamage = (int)(baseDamage * damageMultiplier);
        target.GetComponent<Character>().DecHealth(finalDamage);
    }
}
