﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtMoue : MonoBehaviour
{

    public float offset;

	// Update is called once per frame
	void Update ()
    {
        Vector3 mouseDif = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        transform.eulerAngles = new Vector3(0, 0, Mathf.Atan2(mouseDif.y, mouseDif.x) * Mathf.Rad2Deg + offset);
	}
}
