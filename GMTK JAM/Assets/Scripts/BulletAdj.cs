﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BulletAdj : ScriptableObject
{
    public abstract void DoDamage(GameObject target, int baseDamage);
}
