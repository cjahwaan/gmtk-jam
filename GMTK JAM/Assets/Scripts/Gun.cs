﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public ObjectPool ammoPool;
    public int maxAmmo;
    public int burstCount;
    public float fireRate;
    public float burstDelay;
    public Vector3 bulletOffset;

    float tick = 0;
    int burstCounter;
    float burstClock;
    AudioQue audioQue;

    private void Awake()
    {
        audioQue = GetComponent<AudioQue>();
    }

    // Update is called once per frame
    void Update ()
    {
        EmitCounter();

        if(tick >= 1)
        {
            tick = 0;
            EmitBurst();
        }
	}

    public void PullTrigger()
    {
        tick += Time.deltaTime * fireRate;
    }

    public void ReleaseTrigger()
    {
        tick = 0;
    }

    void Emit()
    {
        GameObject g = ammoPool.getItem();
        g.transform.position = transform.position + new Vector3(bulletOffset.x, bulletOffset.y, 0);
        g.transform.eulerAngles = transform.eulerAngles + new Vector3(0, 0, bulletOffset.z);
        g.SetActive(true);
        audioQue.playQue();
    }

    void EmitBurst()
    {
        if(burstCount > burstCounter)
        {
            burstCounter++;
            Emit();
        }
    }

    void EmitCounter()
    {
        if (burstCount <= burstCounter)
        {
            burstClock += Time.deltaTime;
        }

        if (burstClock >= burstDelay)
        {
            burstClock = 0;
            burstCounter = 0;
        }
    }
}
