﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapBuilder : MonoBehaviour
{

    public ObjectPool pool;
    public BasicMapGenerator generator;
    public GameObject player;
    List<GameObject> rooms;
    GameObject[,] roomPointers;

    public void BuildMap()
    {
        rooms = new List<GameObject>();
        roomPointers = new GameObject[generator.width, generator.height];
        bool[,] map = generator.GetFloorPlan();
        for (int x = 0; x < map.GetLength(0); x++)
        {
            for (int y = 0; y < map.GetLength(1); y++)
            {
                if(map[x, y])
                {
                    GameObject item = pool.getItem();
                    roomPointers[x, y] = item;
                    item.transform.position = new Vector3(x * 20, y * 20, 0);
                    item.GetComponent<RoomManager>().roomID(x, y);
                    item.transform.Find("EnemyManager").GetComponent<EnemyManager>().target = player;
                    item.gameObject.SetActive(true);
                    rooms.Add(item);
                }
            }
        }
    }

    public List<GameObject> GetRooms()
    {
        return rooms;
    }

    public GameObject GetRoomPointer(int x, int y)
    {
        return roomPointers[x, y];
    }

}
