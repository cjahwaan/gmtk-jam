﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnemyDifficulty
{
    Easy = 1, Medium, Hard
}

[System.Serializable]
public struct RoundInfo
{
    public int enemyCount;
    public EnemyDifficulty difficulty;
}

public class EnemyManager : MonoBehaviour
{

    public ObjectPool[] pool;
    public GameObject target;
    public float width, height;
    public RoundInfo roundInfo;

    int index;

    public void BeginRound()
    {
        for (int i = 0; i < roundInfo.enemyCount; i++)
        {
            index = (Random.Range(1, 4) * (int)roundInfo.difficulty) - 1;
            PlaceEnemies();
        }
    }

    void PlaceEnemies()
    {
        GameObject enemy = pool[index].getItem();
        enemy.transform.position = getRandomPosition() + transform.position;
        enemy.GetComponent<EnemyAI>().target = target;
        enemy.GetComponent<Character>().ResetHealth();
        enemy.SetActive(true);
    }

    Vector3 getRandomPosition()
    {
        return new Vector3(Random.Range(-width/2, width/2), Random.Range(-height/2, height/2), 0);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(transform.position, new Vector3(width, height, 0));

        Gizmos.color = new Color(0, 1, 0, 0.15f);
        Gizmos.DrawCube(transform.position, new Vector3(width, height, 0));

    }

    public bool isAllEnemiesDown()
    {
        for (int i = 0; i < pool.Length; i++)
        {
            if(!pool[i].isAllSleep())
            {
                return false;
            }
        }
        return true;
    }
}
