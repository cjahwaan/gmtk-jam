﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomManager : MonoBehaviour
{
    public EnemyManager enemyManager;
    public FloorManager floorManager;
    public GameObject northDoor, southDoor, eastDoor, westDoor;

    public int x, y;

	// Use this for initialization
	void Awake ()
    {
        floorManager = FindObjectOfType<FloorManager>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(enemyManager.isAllEnemiesDown())
        {
            if(floorManager.basicMapGenerator.isThereRoom(x + 1, y))
            {
                eastDoor.gameObject.SetActive(true);
            }

            if (floorManager.basicMapGenerator.isThereRoom(x - 1, y))
            {
                westDoor.gameObject.SetActive(true);
            }

            if (floorManager.basicMapGenerator.isThereRoom(x, y + 1))
            {
                northDoor.gameObject.SetActive(true);
            }

            if (floorManager.basicMapGenerator.isThereRoom(x, y - 1))
            {
                southDoor.gameObject.SetActive(true);
            }

            if(floorManager.player.GetComponent<Collider2D>().IsTouching(eastDoor.GetComponent<Collider2D>()))
            {
                GameObject room = floorManager.builder.GetRoomPointer(x + 1, y);
                Vector3 pos = room.transform.Find("SpawnPoint").transform.position;
                floorManager.player.transform.position = pos;
                floorManager.container.transform.position = pos;
                room.GetComponent<RoomManager>().RoomEntered();
            }

            if (floorManager.player.GetComponent<Collider2D>().IsTouching(westDoor.GetComponent<Collider2D>()))
            {
                GameObject room = floorManager.builder.GetRoomPointer(x - 1, y);
                Vector3 pos = room.transform.Find("SpawnPoint").transform.position;
                floorManager.player.transform.position = pos;
                floorManager.container.transform.position = pos;
                room.GetComponent<RoomManager>().RoomEntered();
            }

            if (floorManager.player.GetComponent<Collider2D>().IsTouching(northDoor.GetComponent<Collider2D>()))
            {
                GameObject room = floorManager.builder.GetRoomPointer(x, y + 1);
                Vector3 pos = room.transform.Find("SpawnPoint").transform.position;
                floorManager.player.transform.position = pos;
                floorManager.container.transform.position = pos;
                room.GetComponent<RoomManager>().RoomEntered();
            }

            if (floorManager.player.GetComponent<Collider2D>().IsTouching(southDoor.GetComponent<Collider2D>()))
            {
                GameObject room = floorManager.builder.GetRoomPointer(x, y - 1);
                Vector3 pos = room.transform.Find("SpawnPoint").transform.position;
                floorManager.player.transform.position = pos;
                floorManager.container.transform.position = pos;
                room.GetComponent<RoomManager>().RoomEntered();
            }
        }
        else
        {
            eastDoor.gameObject.SetActive(false);
            westDoor.gameObject.SetActive(false);
            southDoor.gameObject.SetActive(false);
            northDoor.gameObject.SetActive(false);
        }
	}

    public void RoomEntered()
    {
        enemyManager.BeginRound();
    }

    public void roomID(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
}
