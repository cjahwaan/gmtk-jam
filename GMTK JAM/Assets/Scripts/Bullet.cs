﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float bulletForce;
    public int baseBulletDamage;
    public BulletAdj[] bulletAdjs;

    Rigidbody2D body;
    TeamSys team;
    int finalBulletDamage;

	// Use this for initialization
	void Awake ()
    {
        body = GetComponent<Rigidbody2D>();
        team = GetComponent<TeamSys>();
	}

    void OnEnable()
    {
        body.velocity = Vector2.zero;
        body.AddForce(transform.up * bulletForce);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Wall")
        {
            this.gameObject.SetActive(false);
        }
        else if(team.team != collision.GetComponent<TeamSys>().team)
        {
            for (int i = 0; i < bulletAdjs.Length; i++)
            {
                bulletAdjs[i].DoDamage(collision.gameObject, baseBulletDamage);
            }
            this.gameObject.SetActive(false);
        }
    }
}
