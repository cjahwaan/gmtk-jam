﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorManager : MonoBehaviour {

    public BasicMapGenerator basicMapGenerator;
    public MapBuilder builder;
    public GameObject player;
    public GameObject container;

	// Use this for initialization
	void Start ()
    {
        basicMapGenerator.GenerateFloor();
        builder.BuildMap();
        player.transform.position = builder.GetRooms()[0].transform.position;
        container.transform.position = builder.GetRooms()[0].transform.position;
	}
}
