﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnemyAIState
{
    WAIT, ATTACK, RUN, ROAM
}

public enum RoamState
{
    NORTH = 0, SOUTH, EAST, WEST
}

public class EnemyAI : MonoBehaviour
{
    public GameObject target;
    public float viewDistance;
    public int roamWaitDirectionTime;
    public float drag;
    public Gun gun;

    Rigidbody2D body;
    Character character;

    EnemyAIState enemyAIState;
    RoamState roamState;

	// Use this for initialization
	void Start ()
    {
        enemyAIState = EnemyAIState.ROAM;
        roamState = RoamState.NORTH;
        body = GetComponent<Rigidbody2D>();
        character = GetComponent<Character>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(character.isDead())
        {
            this.gameObject.SetActive(false);
        }
        float lookDirection = Mathf.Rad2Deg * Mathf.Atan2(target.transform.position.y - this.transform.position.y, target.transform.position.x - transform.position.x);
        switch(enemyAIState)
        {
            case EnemyAIState.WAIT:
                if (Vector3.Distance(transform.position, target.transform.position) < viewDistance)
                {
                    enemyAIState = EnemyAIState.ATTACK;
                }
                break;

            case EnemyAIState.ATTACK:
                // TODO: Attack
                transform.eulerAngles = new Vector3(0, 0, lookDirection);
                gun.PullTrigger();
                if (Vector3.Distance(transform.position, target.transform.position) > viewDistance)
                {
                    enemyAIState = EnemyAIState.ROAM;
                    gun.ReleaseTrigger();
                }
                break;

            case EnemyAIState.RUN:
                // TODO: Run
                transform.eulerAngles = new Vector3(0, 0, -lookDirection);
                body.AddForce(transform.up * character.GetSpeed());
                break;

            case EnemyAIState.ROAM:
                // TODO: Roam
                // TODO: Waiting on character Class to get speed
                switch(roamState)
                {
                    case RoamState.NORTH:
                        body.AddForce( Vector3.up * character.GetSpeed());
                        break;

                    case RoamState.SOUTH:
                        body.AddForce( Vector3.down * character.GetSpeed());
                        break;

                    case RoamState.EAST:
                        body.AddForce( Vector3.right * character.GetSpeed());
                        break;

                    case RoamState.WEST:
                        body.AddForce( Vector3.left * character.GetSpeed());
                        break;
                }
                if((int)Time.time % roamWaitDirectionTime == 0)
                {
                    roamState = randomRoam();
                }
                if (Vector3.Distance(transform.position, target.transform.position) < viewDistance)
                {
                    enemyAIState = EnemyAIState.ATTACK;
                }
                break;
        }
	}

    private void FixedUpdate()
    {
        body.AddForce(-body.velocity * drag);
    }

    RoamState randomRoam()
    {
        return (RoamState)Random.Range(0, 4);
    }
}
