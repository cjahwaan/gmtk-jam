﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    public GameObject objectToBePooled;
    public int startAmount;

    List<GameObject> pool;

    private void Awake()
    {
        pool = new List<GameObject>();
        for (int i = 0; i < startAmount; i++)
        {
            var t = Instantiate(objectToBePooled);
            t.SetActive(false);
            pool.Add(t);
        }
    }

    public GameObject getItem()
    {
        for (int i = 0; i < pool.Count; i++)
        {
            if (!pool[i].activeInHierarchy)
            {
                return pool[i];
            }
        }

        var t = Instantiate(objectToBePooled);
        t.SetActive(false);
        pool.Add(t);
        return t;
    }

    public bool isAllSleep()
    {
        for (int i = 0; i < pool.Count; i++)
        {
            if(pool[i].activeInHierarchy)
            {
                return false;
            }
        }

        return true;
    }
}
